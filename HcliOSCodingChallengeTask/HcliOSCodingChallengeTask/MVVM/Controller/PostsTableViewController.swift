//
//  PostsTableViewController.swift
//  HcliOSCodingChallengeTask
//
//  Created by Medikonda AnilKumar on 16/08/21.
//

import UIKit

class PostsTableViewController: UITableViewController {
    
    var imageHeight: CGFloat = 200.0
    
    var viewModel: PostsTablbeViewModal? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    func setup () {        
        
        
        viewModel = PostsTablbeViewModal()
        viewModel?.vc = self
        viewModel?.setup()
        viewModel?.hitGetPostsApi()
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return viewModel?.posts.count ?? 0
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.PostsTableCell, for: indexPath) as? PostsTableCell else {
            return UITableViewCell()
        }
        guard let post = viewModel?.posts[indexPath.row] else {
            return UITableViewCell()
        }
        cell.setupData(model: post, indexPath: indexPath)
        
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = viewModel?.setUpImageHeight(indexPath) ?? 150.0 + CGFloat(imageHeight)
        return  height
    }
    
    
    // MARK: - ScorllView Delegate Methods
    //Pagination
    override   func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("scrollViewDidEndDragging")
        if ((tableView.contentOffset.y + tableView.frame.size.height) >= tableView.contentSize.height)
        {
            viewModel?.hitPaginationApi()
            
        }
    }
    
    
    
}
