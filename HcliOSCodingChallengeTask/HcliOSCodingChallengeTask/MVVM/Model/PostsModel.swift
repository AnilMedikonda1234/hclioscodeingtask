//
//  PostsModel.swift
//  HcliOSCodingChallengeTask
//  Created by Medikonda AnilKumar on 14/08/21.
//


import Foundation

struct PostsModel : Decodable {
    
    var kind : String?
    var data : PostsData?
    var after : String?
    
    enum CodingKeys: String, CodingKey {

        case kind = "kind"
        case data = "data"
        case after = "after"
    }
    
    init(from decoder: Decoder) throws {

        let values = try decoder.container(keyedBy: CodingKeys.self)
        if let valve = try values.decodeIfPresent(String.self, forKey: .kind) {
            kind = valve
        }
        
        if let PostsData = try values.decodeIfPresent(PostsData.self, forKey: .data) {
            data = PostsData
        }
        
        if let str = try values.decodeIfPresent(String.self, forKey: .after) {
            after = str
        }
        
    }
    
}

struct PostsData : Decodable {
    
    let children : [Children]?

    enum CodingKeys: String, CodingKey {
        case children = "children"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        children = try values.decodeIfPresent([Children].self, forKey: .children)
    }

}

struct Children : Decodable {
    let kind : String?
    let data : NewChildData?

    enum CodingKeys: String, CodingKey {
        case kind = "kind"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        kind = try values.decodeIfPresent(String.self, forKey: .kind)
        data = try values.decodeIfPresent(NewChildData.self, forKey: .data)
    }

}


struct NewChildData : Decodable {
    
    var selftext : String?
    var author_fullname : String?
    var title : String?
    var downs : Int?
    var thumbnail_height : Int?
    var name : String?
    var ups : Int?
    var thumbnail : String?
    var num_comments : Int?
    var total_awards_received : Int?
    var link_flair_text_color : String?
    var score : Int?
    var thumbnail_width : Int?



    enum CodingKeys: String, CodingKey {

        case selftext = "selftext"
        case author_fullname = "author_fullname"
        case title = "title"
        case downs = "downs"
        case thumbnail_height = "thumbnail_height"
        case name = "name"
        case ups = "ups"
        case thumbnail = "thumbnail"
        case num_comments = "num_comments"
        case total_awards_received = "total_awards_received"
        case link_flair_text_color = "link_flair_text_color"
        case score = "score"
        case thumbnail_width = "thumbnail_width"


    }

    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        selftext  = try values.decodeIfPresent(String.self, forKey: .selftext)
        author_fullname =  try values.decodeIfPresent(String.self, forKey: .author_fullname)
        title =  try values.decodeIfPresent(String.self, forKey: .title)
        downs =  try values.decodeIfPresent(Int.self, forKey: .downs)
        thumbnail_height =   try values.decodeIfPresent(Int.self, forKey: .thumbnail_height)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        
        
        /*
        if let text = try values.decodeIfPresent(String.self, forKey: .selftext) {
             selftext = text
        }

        if let author = try values.decodeIfPresent(String.self, forKey: .author_fullname) {
            author_fullname = author
        }

        if let titleText = try values.decodeIfPresent(String.self, forKey: .title) {
            title = titleText
        }

        if let downValves = try values.decodeIfPresent(Int.self, forKey: .downs) {
            downs = downValves
        }

        if let height = try values.decodeIfPresent(Int.self, forKey: .thumbnail_height) {
            thumbnail_height = height
        }
        if let imageStr = try values.decodeIfPresent(String.self, forKey: .thumbnail) {
            thumbnail = imageStr
        }

        if let valve = try values.decodeIfPresent(String.self, forKey: .name) {
            name = valve
        }

        if let upvalves = try values.decodeIfPresent(Int.self, forKey: .ups) {
            ups = upvalves
        }
        if let comments = try values.decodeIfPresent(Int.self, forKey: .num_comments) {
            num_comments = comments
        }
        if let awards = try values.decodeIfPresent(Int.self, forKey: .total_awards_received) {
            total_awards_received = awards
        }


        if let linkflair = try values.decodeIfPresent(String.self, forKey: .link_flair_text_color) {
            link_flair_text_color = linkflair
        }

        if let scoreValve = try values.decodeIfPresent(Int.self, forKey: .score) {
            score = scoreValve
        }

        if let width = try values.decodeIfPresent(Int.self, forKey: .thumbnail_width) {
            thumbnail_width = width
        }
        */
        
        
        
    }
    

}
