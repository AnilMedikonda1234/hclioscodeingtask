//
//  PostsViewModal.swift
//  HcliOSCodingChallengeTask
//  Created by Medikonda AnilKumar on 16/08/21.
//

import Foundation

import Foundation
import UIKit

class PostsTablbeViewModal {
    var textRowHeight = 0.0
    weak var vc: PostsTableViewController?
    var welcomeData: PostsModel? = nil
    var posts =  [Children]()
    var index = 0
    var after = ""
    
    var viewColor: UIColor = .black
    var setAlpha: CGFloat = 0.5
    var gifName: String = "demoImage.giftImage"
    
    lazy var transparentView: UIView = {
        let transparentView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 100))
        transparentView.backgroundColor = viewColor.withAlphaComponent(setAlpha)
        transparentView.isUserInteractionEnabled = false
        return transparentView
    }()
    
    lazy var gifImage: UIImageView = {
        var gifImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 200, height: 60))
        gifImage.contentMode = .scaleAspectFit
        gifImage.center = transparentView.center
        gifImage.isUserInteractionEnabled = false
        gifImage.loadGif(name: gifName)
        return gifImage
    }()
    
    
    
    func setup() {
        
        vc?.title = "Posts"
        vc?.tableView.backgroundColor = UIColor(white: 0.95, alpha: 1)
        vc?.tableView.backgroundColor = #colorLiteral(red: 0.8640883565, green: 0.9178858399, blue: 0.9996795058, alpha: 1)
        vc?.tableView.alwaysBounceVertical = true
        // register the cell
        vc?.tableView.register(PostsTableCell.self, forCellReuseIdentifier: Identifier.PostsTableCell)
    }
    
    func hitGetPostsApi () {
        
        CustomLoader.instance.showLoaderView()
        AFWrapper.shared.requestGETURL(Constants.URLs.postsApi, nil) { [weak self] (result) in
            guard let self = self else {return}
            switch result {
            case .success(let data):
                self.updateData(data: data)
            case .failure(let error):
                Utility.shared.infoAlertWithMessage("Error", message: "\(error.localizedDescription)", viewController: self.vc!)
            }
            CustomLoader.instance.hideLoaderView()
            
        }
    }
    
    
    private  func updateData (data: Data) {
        
        do {
            welcomeData = try JSONDecoder().decode(PostsModel.self, from: data)
            
        } catch {
            Utility.shared.infoAlertWithMessage("Error", message: "\(error.localizedDescription)", viewController: vc!)
        }
        
        guard let welcomeData = welcomeData  else {
            return
        }
        guard let childData = welcomeData.data?.children else {
            return
        }
        after = welcomeData.after ?? ""
        posts.append(contentsOf: childData)
        DispatchQueue.main.async {
            self.vc?.tableView.reloadData()
            
        }
        
    }
    
    func hitPaginationApi() {
        
        let pageinationApi = "\(Constants.URLs.paginationApi)\(after)"
        vc?.tableView.tableFooterView = createSpinnerForFootetView()
        AFWrapper.shared.requestGETURL(pageinationApi, nil) { [weak self] (result) in
            guard let self = self else {return}
            DispatchQueue.main.async {
                self.vc?.tableView.tableFooterView = nil
            }
            switch result {
            case .success(let data):
                self.updateData(data: data)
            case .failure(let error):
                Utility.shared.infoAlertWithMessage("Error", message: "\(error.localizedDescription)", viewController: self.vc!)
            }
            
        }
    }
    
    private func createSpinnerForFootetView() -> UIView {
        let footerView = transparentView
        self.transparentView.addSubview(self.gifImage)
        self.transparentView.bringSubviewToFront(self.gifImage)
        return footerView
    }
    
    
    
    // MARK: Functions
    // dymanic change captionTextView size
    func getCaptionTextHeight(text: String) -> CGFloat {
        let rect = NSString(string: text).boundingRect(with: CGSize(width: UIScreen.main.bounds.width, height: 2000),options: NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin),attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)],context: nil)
        return rect.height
    }
    
    // dymanic change postImageView size
    func getPostImageHeight(image: UIImage) -> CGFloat {
        let imageCrop = image.getCropRatio()
        let postImageHeigh = UIScreen.main.bounds.width / imageCrop
        return postImageHeigh
        
    }
    
    func setUpImageHeight(_ indexPath: IndexPath) -> CGFloat {
        let model = posts[indexPath.row]
        let captionText = model.data?.title
        let textHeight = getCaptionTextHeight(text: captionText ?? " ")
        textRowHeight = Double(textHeight + Constants.PostsHeight)
        let imageHeight =  self.imageHeight(indexpath: indexPath)
        return textHeight + imageHeight  + 18
    }
    
    
    func imageHeight(indexpath: IndexPath) -> CGFloat {
        
        let model = posts[indexpath.row]
        
        guard let thumbnailHeight =  model.data?.thumbnail_height ,let thumbnailWidth = model.data?.thumbnail_width else {
            return  UIScreen.main.bounds.width
        }
        
        let aspectRatio = Utility.shared.getCropRatio(width: CGFloat(thumbnailWidth), height: CGFloat(thumbnailHeight))
        return UIScreen.main.bounds.width * aspectRatio
        
    }
    
}







