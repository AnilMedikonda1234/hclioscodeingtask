//
//  AFWrapper.swift
//  HcliOSCodingChallengeTask
//
//  Created by Medikonda AnilKumar on 13/08/21.
//

import Foundation
import UIKit


enum NetworkError: Error {
    case badURL
    case invalidData
    case badrequest
    case noData
 
}


class AFWrapper {
    
    static let shared = AFWrapper()
    private init() {}
    
    func requestGETURL(_ strURL: String, _ params:[String : AnyObject]?,   completionHandler: @escaping (Result<Data, NetworkError>) -> Void) {
        
        guard let url = URL(string: strURL) else {
            completionHandler(.failure(.badURL))
            return
        }
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data , error == nil  else {
                completionHandler(.failure(.noData))
                return
            }
            completionHandler(.success(data))

        }
        dataTask.resume()
    }
    
    
    func getImageHeigh(_ strURL: String?, _ params:[String : AnyObject]?,   completionHandler: @escaping (Result<UIImage?, NetworkError>) -> Void) {
        
        guard let imageURL = URL(string: strURL ?? "" ) else {
             return
         }
         let task = URLSession.shared.dataTask(with: imageURL) { (data, _, error) in
            
             guard let data = data, error == nil else {
                completionHandler(.failure(.noData))
                return
                
             }
             let image = UIImage(data: data)
            completionHandler(.success(image))
         }

         task.resume()
    }
    
  
}
