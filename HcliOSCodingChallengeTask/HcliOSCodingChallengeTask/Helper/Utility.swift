//
//  Utility.swift
//  HcliOSCodingChallengeTask
//
//  Created by Medikonda AnilKumar on 13/08/21.
//

import Foundation
import UIKit

class Utility {
    
    static let shared = Utility()
    
    // MARK: - Alert Methods
    
     func infoAlertWithoutMessage(_ title:String,viewController:UIViewController) -> Void {
        
        let actionSheetController: UIAlertController = UIAlertController(title:title, message:nil, preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title:"OK" , style: .cancel)
        actionSheetController.addAction(cancelAction)
        viewController.present(actionSheetController, animated: true, completion: nil)
        DispatchQueue.main.async(execute: { viewController.present(actionSheetController, animated: true, completion: nil) })
        
    }
    
     func infoAlertWithMessage(_ title:String, message:String,viewController:UIViewController) -> Void {
        let actionSheetController: UIAlertController = UIAlertController(title:title, message:message, preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title:"OK" , style: .cancel) { action -> Void in }
        actionSheetController.addAction(cancelAction)
        DispatchQueue.main.async(execute: { viewController.present(actionSheetController, animated: true, completion: nil) })
    }
    
    
}
