//
//  Constants.swift
//  HcliOSCodingChallengeTask
//
//  Created by Medikonda AnilKumar on 13/08/21.
//

import Foundation
import UIKit

struct Constants {
    
    struct URLs {
        static let  postsApi = "https://www.reddit.com/.json"
    }
    
    struct Images {
        static  let privacyImage = UIImage(named: "icon-globe")
        static  let placeholderImage = UIImage(named: "icon_profile")

        
    }
    
}

struct Identifier {
    
    static let newsfeedCell = "NewsfeedCell"
    
    
}

