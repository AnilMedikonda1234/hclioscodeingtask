//
//  ExtesnionTests.swift
//  HcliOSCodingChallengeTaskTests
//
//  Created by Medikonda AnilKumar on 15/08/21.
//

import XCTest
@testable import HcliOSCodingChallengeTask


class ExtesnionTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testImageCorpRatioTests () {
        let image = #imageLiteral(resourceName: "Mark")
        let ratio = image.getCropRatio()
        XCTAssertNotNil(ratio, "ratio is image nil valve")
        
    }
    
    func testGiftImageNotNil () {
        let image = UIImage.gif(url: "https://cdn.maikoapp.com/3d4b/4quqa/150.png")
        XCTAssertNotNil(image, "Image is Nil Valve")
        
    }
    
    func testGiftImageNil () {
        let image = UIImage.gif(url: "https://cdn.maikoapp.com/3d4b/4quqa")
        XCTAssertNotNil(image, "Image is Nil Valve")
        
    }
    
    func testgetImageFromDataTest () {
        do {
            let data = try Data.init(contentsOf: URL(string: "https://cdn.maikoapp.com/3d4b/4quqa/150.png")!)
            let image = UIImage.gif(data: data)
            XCTAssertNotNil(image, "Image is Nil Valve")
        } catch {
            XCTFail("image data is nil valve")
        }
    }
    
    func testgetImageFromAsset () {
        let image = UIImage.gif(asset: "icon-like")
        XCTAssertNil(image, "Image is Nil Valve")
        
    }
    

}
