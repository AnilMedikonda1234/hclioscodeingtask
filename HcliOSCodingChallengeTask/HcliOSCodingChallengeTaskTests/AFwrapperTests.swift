//
//  AFwrapperTests.swift
//  HcliOSCodingChallengeTaskTests
//  Created by Medikonda AnilKumar on 15/08/21.
//

import XCTest

@testable import HcliOSCodingChallengeTask

class AFwrapperTests: XCTestCase {

override func setUpWithError() throws {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

override func tearDownWithError() throws {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}


func testInValidURLTests () {
    AFWrapper.shared.requestGETURL("InvalidURlTests", nil) {   (result) in
        switch result {
        case .success(let data):
            XCTAssertNil(data, "data is not Nil")
        case .failure(let error):
            XCTAssertNotNil(error, "is valid url Tests")
        }
        
    }
    
}

func testValidApiCallTests () {
    
    AFWrapper.shared.requestGETURL(Constants.URLs.postsApi, nil) {   (result) in
        switch result {
        case .success(let data):
            XCTAssertNotNil(data, "data is nil Valve")
            do {
                let responseData = try JSONDecoder().decode(NewsFeedModel.self, from: data)
                XCTAssertNotNil(responseData, "data is nil Valve")
            } catch {
                XCTFail("Data is nil Valve")            }
    
        case .failure( _):
            XCTFail("Fail")
        }
        
    }
    
}



}
