//
//  NewsFeedViewModalTests.swift
//  HcliOSCodingChallengeTaskTests
//
//  Created by Medikonda AnilKumar on 15/08/21.
//

import XCTest
@testable import HcliOSCodingChallengeTask


class NewsFeedViewModalTests: XCTestCase {
    
    var sut: NewsFeedViewModel!

    override func setUpWithError() throws {
        sut = NewsFeedViewModel()
    }

    override func tearDownWithError() throws {
        sut = nil
    }

    
    func testSetUp () {
        
        sut.setup()
    }

    func testGetCaptureTextHeight ()  {
        
        //Arrange
         let text = "Hi This is Sample Text Height"
        
        //Act
        let height = sut.getCaptionTextHeight(text: text)
        
        //Assert
        XCTAssertNotNil(height, "height is nil Valve")
        
    }
    
    func testCalcualteImageHeight () {
        
        ///Arrange
         let image = #imageLiteral(resourceName: "Mary")
        
        //Act
        let imageheight = sut.getPostImageHeight(image: image)
        
        //Assert
        XCTAssertNotNil(imageheight, "height is nil Valve")
    }

}
