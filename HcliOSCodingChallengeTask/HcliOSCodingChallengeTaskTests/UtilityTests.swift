//
//  UtilityTests.swift
//  HcliOSCodingChallengeTaskTests
//
//  Created by Medikonda AnilKumar on 15/08/21.
//

import XCTest
@testable import HcliOSCodingChallengeTask


class UtilityTests: XCTestCase {
    
    var sut: Utility!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    override func tearDown() {
        sut = nil
    }
    
    func testNotNilUtilityClass() {
        let vc = UIViewController()
        sut = Utility()
        sut.infoAlertWithMessage("Arror", message: "This Message Nill", viewController: vc)
        XCTAssertNotNil(sut, "Sut nilvalve")
        sut.infoAlertWithoutMessage("Error", viewController: vc)
        XCTAssertNotNil(sut, "Sut nil valve")

        
    }
    
    func testCustomLoaderView () {
        CustomLoader.instance.hideLoaderView()
        
    }
    

}
